﻿using System;
using System.Drawing;

namespace Resources.Models
{
    public class Colormodel
    {
        public string Name { get; set; }
        public Color Color { get; set; }
    }
}
