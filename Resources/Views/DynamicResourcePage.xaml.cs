﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Security.Principal;
using System.Text.RegularExpressions;
using Resources.Models;
using Xamarin.Forms;

namespace Resources.Views
{
    public partial class DynamicResourcePage : ContentPage
    {
        public DynamicResourcePage()
        {
            InitializeComponent();

            List<Colormodel> colors = new List<Colormodel>
            {                
                new Colormodel
                {
                    Name = "Blue",
                    Color =  Color.FromHex("#00A0FF") 
                },

                new Colormodel
                {
                    Name = "Orange",
                    Color =  Color.FromHex("#F37721")
                },

                new Colormodel
                {
                    Name = "Purple",
                    Color =  Color.FromHex("#8B40b2")
                },

                new Colormodel
                {
                    Name = "Yellow",
                    Color =  Color.FromHex("#FCD116")
                },
            };


            myPicker.ItemsSource = new List<Colormodel>(colors);

            myPicker.SelectedIndexChanged += MyPicker_SelectedIndexChanged;

        }

        private void MyPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (myPicker.SelectedIndex == -1)
            {
                return;
            }

            App.Current.Resources["DefaultColor"] = ((Colormodel)myPicker.SelectedItem).Color;
        }

        async void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            await App.Current.MainPage.Navigation.PushAsync(new IconsPage());
        }
    }
}
